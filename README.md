# Amazon Lookout for Metrics Connector
For an introduction to the service with tutorials for getting started, visit "https://docs.aws.amazon.com/lookoutmetrics/latest/dev" Amazon Lookout for Metrics Developer Guide.

Documentation: https://docs.aws.amazon.com/lookoutmetrics

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/lookoutmetrics/2017-07-25/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

